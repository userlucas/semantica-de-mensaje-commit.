# Semántica de mensaje en tu commit.

Su mensaje de confirmación puede convertirlo en un mejor programador 👨‍💻 . 

## ✍ Ejemplo:

```
Shortcut: 
gac b "add fix code in ..."

Result: 
BUG FIX: add fix code in ...
^------^ ^-----------------^
|         |
|         +-> Resumen en tiempo presente..
|
+-------> Type: chore, docs, feat, fix, refactor, style, test and working on.
```

Más ejemplos:

-   `feat`: (Característica nueva para el usuario, no una característica nueva para el script de compilación)
-   `fix`: (Corrección de errores para el usuario, no una corrección de un script de compilación)
-   `docs`: (Cambios en la documentación)
-   `style`: (Formato, punto y coma faltantes, etc; sin cambios en el código de producción)
-   `refactor`: (Refactorización del código de producción, por ejemplo, cambiar el nombre de una variable)
-   `test`: (Agregando pruebas faltantes, refactorizando pruebas; sin cambios en el código de producción)
-   `chore`: (Actualización de tareas rutinarias, etc., sin cambios en el código de producción)
-   `working on`: (Trabajando en una sección o tarea en específico, etc.)

## Instalación

### Mac OS

1.  Necesitas un `~/.zshrc`archivo
2.  Ábrelo o créalo: `vim ~/.zshrc`
3.  Ingrese al modo de inserción: `i`
4.   Pega al final de su archivo `~/.zshrc`  el contenido de GAC SH
5.  Salir de vim: `:wq`
6.  Reinicia tu terminal
7.  Disfrutar de más rápido y con formato `git add`y `git commit`acciones

### Linux

Funciona igual que macOS, ó utilice `~/.profile` archivo en su lugar.

## GAC SH
```
#Git con gac

function gac() {

  if [ $# -eq 0 ] || [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
    # displays help with
    # gac | gac -h | gac --help
    echo "------"
    echo "Cannot commit without comments. Semantic reminder:"
    echo "🐛 BUG FIX:       b" 
    echo "📦 CHORE:         c" 
    echo "📖 DOCS:          d" 
    echo "✅ FEAT:          f" 
    echo "🚀 NEW RELEASE:   n" 
    echo "👌 IMPROVE:       i"
    echo "🪚 REFACTOR:      r" 
    echo "🎨 STYLE:         s"
    echo "🧪 TEST:          t"
    echo "🛠  WORKING ON:    w"
    echo "------"
    return 1
  fi  

  SHORTCUT=$1
  shift ;
  COMMENT=$@


  # Fix a bug
  if [ "$SHORTCUT" = "b" ]; then
    SHORTCUT="BUG FIX:"

  # Chore
  elif [ "$SHORTCUT" = "c" ]; then
    SHORTCUT="CHORE:"

  # Write or edit existing documentation
  elif [ "$SHORTCUT" = "d" ]; then
    SHORTCUT="DOCS:"

  # Add new feature
  elif [ "$SHORTCUT" = "f" ]; then
    SHORTCUT="FEAT:"

  # Deploy in production
  elif [ "$SHORTCUT" = "n" ]; then
    SHORTCUT="NEW RELEASE:"
  
  # Improve your code base
  elif [ "$SHORTCUT" = "i" ]; then
    SHORTCUT="IMPROVE:"

  # Refator your code base
  elif [ "$SHORTCUT" = "r" ]; then
    SHORTCUT="REFACTOR:"

  # Styling actions
  elif [ "$SHORTCUT" = "s" ]; then 
    SHORTCUT="STYLE:"

  # Test your code
  elif [ "$SHORTCUT" = "t" ]; then 
    SHORTCUT="TEST:"

  # Working on a feature
  elif [ "$SHORTCUT" = "w" ]; then 
    SHORTCUT="WORKING ON:"
  fi
  
  # res with or without semantic
  git commit -m "$SHORTCUT $COMMENT"
  return 1
}

```

Referencias:

 - https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716
 - https://github.com/devpolo/gac
